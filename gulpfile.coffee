"use strict"

# args & help
args = require('yargs')
  .usage('Command line tool to build MPX frontend application. Usage: $0 <command>')
  .demand(1)
  .example('$0 pre::build', 'Builds bower component assets')
  .example('$0 build', 'Builds application (without bower components)')
  .example('$0 server', 'Builds and runs http server with livereload')
  .example('$0 test', 'Runs single test cycle')
  .example('$0 dist', 'Builds production application')
  .boolean('full').describe('full', 'option for build, server command, runs pre::build in the beginning')
  .boolean('dist').describe('dist', 'option for server command, runs full production build cycle')
  .boolean('watch').describe('watch', 'option for test command, enable autowatching ')
  .string('browsers').describe('browsers', 'option for test command, specify browsers for tests, comma separated list')
  .argv

# Require modules
gulp    = require("gulp")
_       = require('lodash')
historyApiFallback = require('connect-history-api-fallback')

# Get args
isProduction = args.e == 'production'

# Preset variables
app  = 'app'
tmp  = 'generated'
dist = 'dist'

ENV       = null
browsers  = ['Chrome']

isDevelopment = false
isTest        = false
isProduction  = false

isLive = !!args.watch
isDist = !!args.dist
isFull = !!args.full

switch args._[0]
  when 'test'
    ENV = 'test'
    tmp = '.test'
    isTest = true

  when 'dist'
    ENV = 'production'
    tmp = '.dist'
    isProduction = true

  else
    ENV = 'development'
    isDevelopment = true

unless _.isEmpty(args.browsers)
  browsers = _.map args.browsers.split(','), (x) -> x.trim()

sassConfig =
  style: if isProduction then "compact" else "expanded",
  loadPath: [
    "bower_components"
  ],
  compass: true,
  trace: false,
  debugInfo: false,
  sourcemap: false

# imageminConfig =
#   optimizationLevel: 5

serverConfig =
  root: if isDist then [dist] else [tmp]
  port: if isDist then 9001 else 9000
  livereload: true
  middleware: (connect, opt) -> [ historyApiFallback ]

# Utils
$        = require("gulp-load-plugins")(lazy: false)
$run     = require('run-sequence')
$q       = require('q')
$logger  = $.util.log
$exec    = require('child_process').exec

autoprefixer = $.autoprefixer("last 2 versions", "Android 4", "Explorer 10", "Safari 7")

# Filters
jsFilter    = $.filter(['**/*.js', '!**/*.min.js'])
cssFilter   = $.filter(['**/*.css', '!**/*.min.css'])
scssFilter  = $.filter('**/*.scss')
styleFilter = $.filter(['**/*.scss', '**/*.css', '!**/*.min.css'])
imgFilter   = $.filter(['**/*.jpg', '**/*.png', '**/*.gif'])
fontFilter  = $.filter(['**/*.eot', '**/*.svg', '**/*.ttf', '**/*.woff', '**/*.otf'])
scssPartialFilter = $.filter('**/_*.scss')

########### HELPER METHODS ############
collectSources = (path) ->
  path = gulp.src(path) if _.isString(path) || _.isArray(path)
  path

collectAndBuildCoffee = (path, name, dest, options) ->
  options ?= {}

  collectSources(path)
  .pipe($.plumber(errorHandler: $.notify.onError("Error: <%= error.message %>")))
  .pipe($.coffee({bare: false, sourceMap: false}).on('error', $logger))
  .pipe($.cond(!options.noHint, $.jshint(".jshintrc")))
  .pipe($.cond(!options.noHint, $.jshint.reporter('jshint-stylish')))
  .pipe($.cond(!!name, $.concat(name || 'undefined.concat')))
  .pipe($.insert.prepend("'use strict';\n"))
  .pipe($.cond(!!dest, gulp.dest(dest)))
  .pipe($.cond(!!dest, $.size(showFiles: true)))

collectAndBuildJs = (path, name, dest, options) ->
  options ?= {}

  collectSources(path)
  .pipe($.plumber(errorHandler: $.notify.onError("Error: <%= error.message %>")))
  .pipe($.cond(!options.noHint, $.jshint(".jshintrc")))
  .pipe($.cond(!options.noHint, $.jshint.reporter('jshint-stylish')))
  .pipe($.cond(!!name, $.concat(name || 'undefined.concat')))
  .pipe($.cond(!!dest, gulp.dest(dest)))
  .pipe($.cond(!!dest, $.size(showFiles: true)))

collectAndBuildSass = (path, name, dest, options) ->
  options ?= {}

  collectSources(path)
  .pipe($.plumber(errorHandler: $.notify.onError("Error: <%= error.message %>")))
  .pipe($.rubySass(sassConfig).on('error', $logger))
  .pipe($.cond(!!name, $.concat(name || 'undefined.concat')))
  .pipe($.cond(!!dest, gulp.dest(dest)))
  .pipe($.cond(!!dest, $.size(showFiles: true)))

collectAndBuildCss = (path, name, dest, options) ->
  options ?= {}

  collectSources(path)
  .pipe($.plumber(errorHandler: $.notify.onError("Error: <%= error.message %>")))
  # .pipe($.csscomb())
  .pipe(autoprefixer)
  .pipe($.cond(!!name, $.concat(name || 'undefined.concat')))
  .pipe($.cond(!!dest, gulp.dest(dest)))
  .pipe($.cond(!!dest, $.size(showFiles: true)))

collectAndBuildJade = (path, name, dest, options) ->
  options ?= {}

  collectSources(path)
  .pipe($.plumber(errorHandler: $.notify.onError("Error: <%= error.message %>")))
  .pipe($.insert.prepend("doctype html\n"))
  .pipe($.jade())
  .pipe($.angularTemplatecache(name, {standalone:true, root: '/views/'} ))
  .pipe(gulp.dest(dest))
  .pipe($.size(showFiles: true))

doCleanup = (path) ->
  gulp.src(tmp, {read: false})
  .pipe($.clean())

##################################################

# APP core
gulp.task "build::app::root", ->
  scripts = [
    "app/scripts/app.coffee"
    "app/scripts/routes.coffee"
    "app/scripts/config.coffee"
  ]

  src = gulp.src(scripts).pipe($.ngClassify())

  collectAndBuildCoffee(src, "app.js", "#{tmp}/scripts")

# gulp.task "build::app::constants", ->
#   gulp.src("config/#{ENV}.json")
#   .pipe($.ngConstant({
#     name: 'mpx.constants'
#     deps: []
#   }))
#   .pipe($.rename({ basename: 'constants', extname: ".js" }))
#   .pipe(gulp.dest("#{tmp}/scripts"))

# APP scripts: controllers, directives, factories, filters, models, services
appScripts = ['constants', 'controllers', 'factories', 'filters', 'models', 'services', 'directives']
appScriptTasks = [
  "build::app::root",
  # "build::app::constants"
]

appScripts.forEach (script) ->
  appScriptTasks.push("build::app::#{script}")
  gulp.task "build::app::#{script}", ->
    src = gulp.src("app/scripts/#{script}/**/*.coffee")
    .pipe($.ngClassify())

    collectAndBuildCoffee(src, "#{script}.js", "#{tmp}/scripts", noHint: true)

gulp.task "build::app::scripts", appScriptTasks

# APP styles
gulp.task "build::app::styles", ->
  collectAndBuildSass("app/styles/*.scss", null, "#{tmp}/styles")

# JADE templates
gulp.task "build::app::jade", ->
  collectAndBuildJade("app/views/**/*.jade", "templates.js", "#{tmp}/scripts")

# APP index
gulp.task "build::app::index", ->
  gulp.src('app/*.html')
  .pipe(gulp.dest(tmp))
  .pipe($.size(showFiles: true))

# APP assets: images, fonts, data
gulp.task "build::app::images", ->
  gulp.src('app/images/**/*')
  .pipe(gulp.dest("#{tmp}/images"))

gulp.task "build::app::fonts", ->
  gulp.src('app/fonts/**/*')
  .pipe(gulp.dest("#{tmp}/fonts"))

gulp.task "build::app::other", ->
  gulp.src(['app/*.ico', 'app/*.txt'])
  .pipe(gulp.dest(tmp))

gulp.task "build::app::data", ->
  gulp.src('app/data/**/*')
  .pipe(gulp.dest("#{tmp}/data"))

gulp.task "build::app::assets", [
  'build::app::images', 'build::app::fonts', 'build::app::data', 'build::app::other'
]

# JOINT APP TASK
gulp.task "build::app", [
  "build::app::root",
  "build::app::scripts",
  "build::app::styles",
  "build::app::jade",
  "build::app::index",
  "build::app::assets"
]

# BOWER COMPONENTS, files should be listed in bower.json
scriptFilePrinter = (p) -> "build::bower::script: #{p}"
cssFilePrinter    = (p) -> "build::bower::styles (CSS): #{p}"
scssFilePrinter   = (p) -> "build::bower::styles (SCSS): #{p}"
styleFilePrinter  = (p) -> "build::bower::styles (ALL): #{p}"
imgFilePrinter    = (p) -> "build::bower::img: #{p}"
scssPartialFilePrinter = (p) -> "build::bower::styles (_partial): #{p}"
fontFilePrinter = (p) -> "build::bower::fonts: #{p}"

gulp.task "build::bower::scripts", ->
  path = $.bowerFiles().pipe(jsFilter)
  .pipe($.print(scriptFilePrinter))

  collectAndBuildJs(path, 'bower.js', "#{tmp}/scripts", {noHint: true})

# gulp.task "build::bower::js", ->
#   path = $.bowerFiles().pipe(jsFilter)
#   # .pipe($.order([
#   #   "*.js"
#   #   "*angular*"
#   #   "*(lodash|URI|moment|json3|jquery|).js)"
#   # ]))
#   .pipe($.print(jsFilePrinter))

  collectAndBuildJs(path, 'bower.js', "#{tmp}/scripts", {noHint: true})

# gulp.task "build::bower::css", ->
#   path = $.bowerFiles().pipe(cssFilter)
#   # .pipe($.order([
#   #   "vendor/js1.js",
#   #   "vendor/**/*.js",
#   #   "app/coffee1.js",
#   #   "app/**/*.js"
#   # ]))
#   .pipe($.print(cssFilePrinter))

#   collectAndBuildCss(path, 'bower.css', "#{tmp}/styles")

gulp.task "build::bower::styles", ->
  $.bowerFiles()
  .pipe($.plumber(errorHandler: $.notify.onError("Error: <%= error.message %>")))

  # # print all style files
  # .pipe(styleFilter)
  # .pipe($.print(styleFilePrinter))
  # .pipe(styleFilter.restore())

  # select sass partials and rename it
  .pipe(scssPartialFilter)
  .pipe($.print(scssPartialFilePrinter))
  .pipe($.rename({prefix: 'partial_'}))
  .pipe(scssPartialFilter.restore())

  # select scss and compile
  .pipe(scssFilter)
  .pipe($.print(scssFilePrinter))
  .pipe($.rubySass(sassConfig).on('error', $logger))
  .pipe(scssFilter.restore())

  # select css and compiled css
  .pipe(cssFilter)
  .pipe($.print(cssFilePrinter))
  .pipe(autoprefixer)

  .pipe($.concat("bower.css"))

  # replace relative paths to absolute
  # for images
  .pipe($.replace(/url\(['"\s]*.*\/(.*)\.(jpg|png|gif)['"\s]*\)/gi, "url(\"/images/$1.$2\")"))

  # for fonts
  .pipe($.replace('../fonts/', '/fonts/'))

  # replace relative paths to absolute
  # for images
  .pipe($.replace(/url\(['"\s]*.*\/(.*)\.(jpg|png|gif)['"\s]*\)/gi, "url(\"/images/$1.$2\")"))

  # for fonts
  .pipe($.replace('../fonts/', '/fonts/'))

  .pipe(gulp.dest("#{tmp}/styles"))
  .pipe($.size(showFiles: true))

gulp.task "build::bower::images", ->
  $.bowerFiles().pipe(imgFilter)
  .pipe($.print(imgFilePrinter))
  .pipe(gulp.dest("#{tmp}/images/bower"))

gulp.task "build::bower::fonts", ->
  $.bowerFiles().pipe(fontFilter)
  .pipe($.print(fontFilePrinter))
  .pipe($.rename({dirname: ''}))
  .pipe(gulp.dest("#{tmp}/fonts"))

gulp.task "build::bower", ["build::bower::scripts", "build::bower::styles", "build::bower::images", "build::bower::fonts"]

# VENDOR COMPONENTS
# custom build scenarios

## Twitter bootstrap
gulp.task "build::vendor::bootstrap::styles", ->
  collectAndBuildSass('vendor_components/bootstrap/**/*.scss', 'bootstrap.css', "#{tmp}/tmp/styles")

gulp.task "build::vendor::bootstrap::scripts", ->

## Core admin
gulp.task "build::vendor::core_admin::styles", ->
  collectAndBuildSass('vendor_components/core-admin/**/*.scss', 'coreadmin.css', "#{tmp}/tmp/styles")

  # replace relative paths to absolute
  # for images
  .pipe($.replace(/url\(['"\s]*.*\/(.*)\.(jpg|png|gif)['"\s]*\)/gi, "url(\"/images/$1.$2\")"))

  # for fonts
  .pipe($.replace('../fonts/', '/fonts/'))

  .pipe(gulp.dest("#{tmp}/tmp/styles"))

gulp.task "build::vendor::core_admin::scripts", ->

gulp.task "build::vendor::core_admin::images", ->
  gulp.src('vendor_components/core-admin/images/**/*')
  .pipe(gulp.dest("#{tmp}/images"))

gulp.task "build::vendor::core_admin::fonts", ->
  gulp.src('vendor_components/core-admin/fonts/**/*')
  .pipe(gulp.dest("#{tmp}/fonts"))

## vendor assets aggregation
gulp.task "build::vendor::merge_styles", ->
  gulp.src([
    "#{tmp}/tmp/styles/bootstrap.css"
    "#{tmp}/tmp/styles/coreadmin.css"
  ])
  .pipe($.concat('vendor.css'))
  .pipe(gulp.dest("#{tmp}/styles"))
  .pipe($.size(showFiles: true))

gulp.task "build::vendor", (callback) ->
  $run(
    ["build::vendor::core_admin::images", "build::vendor::core_admin::fonts"],
    ["build::vendor::bootstrap::styles", "build::vendor::core_admin::styles"],
    # ["build::vendor::bootstrap::styles"],
    ["build::vendor::merge_styles"],
    callback
  )

# gulp.task "html::build::dist", ->
#   gulp.src("#{tmp}/**/*.html")
#   .pipe($.usemin({
#     css: [$.minifyCss(), $.rev()],
#     html: [$.minifyHtml({empty: true})],
#     js: [$.uglify(), $.rev()]
#   }))
#   .pipe(gulp.dest(dist))

#### CLEAN ####
gulp.task "clean", ->
  doCleanup(tmp)

#### BUILD ####

gulp.task "build", (callback) ->
  $run(["build::app"], callback)

gulp.task "pre::build", (callback) ->
  $run("clean", ["build::bower", "build::vendor"], callback)

#### SERVER ####
gulp.task "connect", $.connect.server(serverConfig)

gulp.task "watch", ->
  dir = if isDist then dist else tmp

  gulp.watch [
    "#{dir}/*.html"
    "#{dir}/scripts/*.js"
    "#{dir}/styles/*.css"
    "#{dir}/views/*.js"
  ], (event) ->
    $logger "Event fired for: #{event.path}"
    gulp.src(event.path).pipe $.connect.reload()

  gulp.watch "#{app}/styles/**/*.scss", ["build::app::styles"]

  gulp.watch "#{app}/scripts/**/*.coffee", ["build::app::scripts"]

  gulp.watch "#{app}/*.html", ["build::app::index"]

  gulp.watch "#{app}/views/**/*.jade", ["build::app::jade"]

  if isDist
    gulp.watch [
      "#{tmp}/**/*.js"
      "#{tmp}/**/*.css"
      "#{tmp}/**/*.html"
    ], ["build::dist::app"]

  return


# #################################################
# gulp.task "build::test::html", ->
#   gulp.src('app/views/**/*.html')
#   .pipe(gulp.dest("#{tmp}/views"))

# gulp.task "build::test::scripts", ->
#   src = [
#     "test/helpers/**/*.coffee"
#     "test/spec/**/*.coffee"
#   ]

#   collectAndBuildCoffee(src, null, "#{tmp}/test", { noHint: true, ngmin: true })

# gulp.task "build::test", ["build::test::html", "build::test::scripts"]

# karmaSrc = [
#   "#{tmp}/scripts/bower.js"
#   'app/bower_components/angular-mocks/angular-mocks.js'
#   "#{tmp}/scripts/constants.js"
#   "#{tmp}/scripts/providers.js"
#   "#{tmp}/scripts/app.js"
#   "#{tmp}/scripts/*.js"

#   "#{tmp}/test/ui_router_noop.js"
#   "#{tmp}/test/**/*.js",

#   "#{tmp}/views/**/*.html"
# ]

# gulp.task "test::karma", ->
#   gulp.src(karmaSrc)
#   .pipe($.plumber(errorHandler: $.notify.onError("Error: <%= error.message %>")))
#   .pipe($.karma({
#     configFile: 'karma.conf.js'
#     action: 'run',
#     browsers: browsers
#   }))
#   .on('error', (err) -> throw err)

# gulp.task "test::karma::live", ->
#   gulp.src(karmaSrc)
#   .pipe($.plumber(errorHandler: $.notify.onError("Error: <%= error.message %>")))
#   .pipe($.karma({
#     configFile: 'karma.conf.js'
#     action: 'run',
#     browsers: browsers
#   }))
#   .on('error', (err) -> @emit('end'))

# gulp.task "watch::test", ->
#   gulp.watch "#{app}/scripts/**/*.coffee", ["build::app::scripts"]
#   gulp.watch "test/**/*.coffee", ["build::test"]
#   gulp.watch "#{tmp}/**/*.js", ["test::karma::live"]
#   return

####################################################################
gulp.task "clean::dist", ->
  doCleanup(dist)

gulp.task "build::dist::app", ->
  gulp.src("#{tmp}/*.html")
  .pipe($.usemin({
    css:  [$.minifyCss(keepBreaks:true), 'concat']
    html: []
    js:   [$.uglify(), $.rev()]
    ng:   [$.uglify(), $.rev()]
    jst:  [$.uglify(), $.rev()]
  }))
  .pipe(gulp.dest(dist))
  .pipe($.size(showFiles: true))

gulp.task "build::dist::images", ->
  gulp.src("#{tmp}/images/**/*")
  #.pipe($.imagemin(imageminConfig))
  .pipe(gulp.dest("#{dist}/images"))
  .pipe($.size(showFiles: true))

gulp.task "build::dist::fonts", ->
  gulp.src("#{tmp}/fonts/**/*")
  .pipe(gulp.dest("#{dist}/fonts"))
  .pipe($.size(showFiles: true))

gulp.task "build::dist::other", ->
  gulp.src(["#{tmp}/*.ico", "#{tmp}/*.txt"])
  .pipe(gulp.dest(dist))

gulp.task "build::dist", [
  "build::dist::app",
  "build::dist::images",
  "build::dist::fonts",
  "build::dist::other"
]

####################################################################

# gulp pre::build - builds bower components
gulp.task "pre::build", (callback) ->
  $run("clean", "build::bower", "build::vendor", callback)

# gulp build - builds app
# gulp build --full - pre builds, build & watch
gulp.task "build", (callback) ->
  if isFull
    $run('clean', 'build::bower', 'build::app', callback)
  else
    $run('build::app', callback)

# gulp server - build & watch
# gulp server --full - pre builds, build & watch
# gulp server --dist - same as previous but for production
gulp.task "server", (callback) ->
  if isDist
    $run("dist", "connect", "watch", callback)
  else
    $run("build", "connect", "watch", callback)

# gulp test - single run
# gulp test --watch - wathjes files
# gulp test --browers=Filefox,Chrome
gulp.task "test", (callback) ->
  tasks = [
    # "build::bower::scripts"
    # "build::app::root"
    # "build::app::scripts"
    # "build::app::html"
    # "build::test"
  ]

  testTask  = if isLive then "watch::test" else "test::karma"

  $run("clean", tasks, testTask, callback)

# gulp dist - builds a production release
gulp.task "dist", (callback) ->
  $run(["clean", "clean::dist"], "pre::build", "build", "build::dist", callback)
