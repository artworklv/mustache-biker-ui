require 'csv'

# source data
# 0: lat
# 1: lon
# 2: timestamp
# 3: x
# 4  y
# 5  z

module Aggregator
  extend self

  AGGREGATION_STEP = 10

  def aggregate(data)
    data = calculate_deltas(data)
    data = reduce(data)
    data = summarize(data)
    data = normalize(data)

    data
  end

  ##############

  def normalize(data)
    maximum = find_maximum(data)

    data.map do |x|
      [
       x[0],
       x[1],
       x[2],
       normalizeTo(x[3], 0, maximum, 0, 1)
      ]
    end
  end

  def find_maximum(data)
    max = 0

    data.each do |x|
      max = x[3] if x[3] > max
    end

    max
  end

  def summarize(data)
    data.map do |x|
      [
       x[0],
       x[1],
       x[2],
       x[3] + x[4] + x[5]
      ]
    end
  end

  def reduce(data)
    len = data.size
    last_index = len - 1

    buffer = []

    0.step(last_index, AGGREGATION_STEP) do |i|
      slice  = data.slice(i, AGGREGATION_STEP)
      mid = slice[slice.size / 2]
      size = slice.size

      avg_x = slice.reduce(0){ |sum, x| sum += x[3]; sum } / size
      avg_y = slice.reduce(0){ |sum, x| sum += x[4]; sum } / size
      avg_z = slice.reduce(0){ |sum, x| sum += x[5]; sum } / size

      buffer << [mid[0], mid[1], mid[2], avg_x, avg_y, avg_z]
    end

    buffer
  end

  def calculate_deltas(data)
    prev = [0,0,0]

    data.map do |x|
      val = [
             x[0],                 # lat
             x[1],                 # lon
             x[2],
             (x[3] - prev[0]).abs, # da
             (x[4] - prev[1]).abs, # db
             (x[5] - prev[2]).abs, # dc
            ]

      prev = [x[3], x[4], x[5]]

      val
    end
  end

  def normalizeTo(x, xmin, xmax, ymin, ymax)
    xrange = xmax - xmin
    yrange = ymax - ymin
    ymin + (x - xmin) * (yrange.to_f / xrange)
  end
end


raw = CSV.read('sample.csv', col_sep: "\t")

data = raw.map do |row|
  [
   row[4].to_f,
   row[3].to_f,
   0,
   row[0].to_i,
   row[1].to_i,
   row[2].to_i
  ]
end

data = Aggregator.aggregate(data)

data = data.map do |x|
  [
   x[0],
   x[1],
   x[3],
   x[2]
  ]
end

str = <<-EOL
class HeatFixtureTwo extends Service
  constructor: ($q) ->

    @all = ->
      deferred = $q.defer()
      deferred.resolve(DATA)
      deferred.promise

    @get = ->
      DATA

    DATA = #{data.inspect}

EOL

File.open('app/scripts/services/fixtures/heat_fixture_two.coffee', 'w+') do |f|
  f.write(str)
end

p data.count
