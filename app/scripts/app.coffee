class App extends App
  @constructor = [
    'templates',
    'ui.router',
    'restangular',
    'leaflet-directive',
    'nvd3ChartDirectives',
    'ngAnimate',
    'mgcrea.ngStrap'
  ]
