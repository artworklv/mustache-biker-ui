class ToFixed extends Filter
  constructor: ->
    return (value, precision = 2) ->
      return 0 unless value
      value.toFixed(precision)
