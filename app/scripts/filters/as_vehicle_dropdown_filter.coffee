class AsVehicleDropdown extends Filter
  constructor: ->
    return (vehicles) ->
      _.map vehicles, (vehicle) ->
        text: vehicle.name
        href: "/vehicles/#{vehicle.id}/tracks/last"
