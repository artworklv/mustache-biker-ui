class AsTrackDropdown extends Filter
  constructor: ->
    return (tracks) ->
      return [] if _.isEmpty(tracks)

      tracks = _.sortBy tracks, (x) -> x.id |0

      list = _.map tracks, (track) ->
        text: "Track ##{track.id}"
        href: "/tracks/#{track.id}"

      list
