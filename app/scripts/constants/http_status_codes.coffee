class HttpStatusCodes extends Constant
  @constructor =
    '401': 'Unauthorized'
    '403': 'Forbidden'
    '404': 'Not Found'
