class CarFixture extends Service
  constructor: ($q) ->

    VEHICLES = [
      { id: 1, name: "KTM" },
      { id: 2, name: "Scott" },
      { id: 3, name: "Hipster Bike" }
    ]

    @find = (id) ->
      id = id |0

      deferred = $q.defer()

      if (found = _.find VEHICLES, (x) -> x.id == id)
        deferred.resolve(found)
      else
        deferred.reject({error: 'NotFound', code: 404})

      deferred.promise

    @all = ->
      deferred = $q.defer()
      deferred.resolve(VEHICLES)
      deferred.promise
