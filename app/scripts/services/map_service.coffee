class Map extends Service
  constructor: () ->
    config =
      defaults: {}

      layers:
        baselayers:
          here:
            name: 'HERE map service'
            url: 'http://3.maptile.lbs.ovi.com/maptiler/v2/maptile/newest/normal.day/{z}/{x}/{y}/256/png8?app_id=2yKW2JPHbywDFRH361e2&token=bfvFGurgrfG15DmHloXZxQ'
            type: 'xyz'
          osm:
            name: 'OpenStreetgMap',
            url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            type: 'xyz'

        #overlays:
          # heatmap:
          #   name: "Heat Map"
          #   type: "heatmap"
          #   data: heatFixtureTwoService.get()
          #   visible: true
          #   layerParams: {}
          #   layerOptions: {size: 10}

    @build = (options) ->
      conf = _.extend({}, config)
