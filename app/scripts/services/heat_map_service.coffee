class HeatMap extends Service
  constructor: () ->
    EasingFunctions =
      # no easing, no acceleration
      linear: (t) -> t

      # accelerating from zero velocity
      easeInQuad: (t) -> t * t

      easeInA: (t) ->
        0.08

      # decelerating to zero velocity
      easeOutQuad: (t) -> t * (2 - t)

      # acceleration until halfway, then deceleration
      easeInOutQuad: (t) -> (if t < 0.5 then 2 * t * t else -1 + (4 - 2 * t) * t)

      # accelerating from zero velocity
      easeInCubic: (t) -> t * t * t

      # decelerating to zero velocity
      easeOutCubic: (t) -> (--t) * t * t + 1

      # acceleration until halfway, then deceleration
      easeInOutCubic: (t) -> (if t < .5 then 4 * t * t * t else (t - 1) * (2 * t - 2) * (2 * t - 2) + 1)

      # accelerating from zero velocity
      easeInQuart: (t) -> t * t * t * t

      # decelerating to zero velocity
      easeOutQuart: (t) -> 1 - (--t) * t * t * t

      # acceleration until halfway, then deceleration
      easeInOutQuart: (t) -> (if t < .5 then 8 * t * t * t * t else 1 - 8 * (--t) * t * t * t)

      # accelerating from zero velocity
      easeInQuint: (t) -> t * t * t * t * t

      # decelerating to zero velocity
      easeOutQuint: (t) -> 1 + (--t) * t * t * t * t

      # acceleration until halfway, then deceleration
      easeInOutQuint: (t) ->
        (if t < .5 then 16 * t * t * t * t * t else 1 + 16 * (--t) * t * t * t * t)

    @build = (data) ->
      _.map data, (x) ->
        [x[1], x[0], EasingFunctions.easeInA(x[3]) ]
