class MapTrack extends Service
  constructor: () ->
    latLngs = (list) ->
      i        = 0
      listSize = list.length
      latlngs  = []

      while i < listSize
        latlngs.push(
          lat: list[i],
          lng: list[i+1]
        )
        i += 2

      latlngs

    @build = (track) ->
      latlngs = latLngs(track.track)

      tracks:
        main:
          color: 'red'
          weight: 4
          latlngs: latlngs
      center:
        lat: 52.5057052402  # latlngs[latlngs.length / 2].lat
        lng: 13.3931966499   # latlngs[latlngs.length / 2].lng
        zoom: track.zoom || 18

    # $scope.center =
    #   lat: trackPoints[(trackPoints.length / 2) |0].lat
    #   lng: trackPoints[(trackPoints.length / 2) |0].lng
    #   zoom: 12
