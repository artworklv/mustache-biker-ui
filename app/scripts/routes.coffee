class Routes extends Config
  constructor: ($stateProvider, $urlRouterProvider) ->
    $urlRouterProvider.otherwise '/tracks/1'

    $stateProvider
    .state 'root',
      templateUrl: '/views/layouts/root.html'

    # .state 'root.index',
    #   url: '/'
    #   templateUrl: '/views/index.html'
    #   controller: 'rootController'

    .state 'root.login',
      url: '/login'
      templateUrl: '/views/login.html'
      controller: 'loginController'

    .state 'root.tracks',
      url: '/tracks/:id'
      templateUrl: '/views/demo.html'
      controller: 'demoController'

    # .state 'root.vehicles',
    #   url: '/vehicles'
    #   templateUrl: '/views/vehicles/index.html'
    #   controller: 'vehiclesIndexController'

    # .state 'root.vehicle',
    #   url: '/vehicles/:id'
    #   template: 'vehiclesShowController'
    #   controller: 'vehiclesShowController'

    # .state 'root.vehicle_tracks',
    #   url: '/vehicles/:vehicle_id/tracks'
    #   template: 'tracksController'
    #   controller: 'tracksIndexController'

    # .state 'root.vehicle_track',
    #   url: '/vehicles/:vehicle_id/tracks/:id'
    #   templateUrl: '/views/tracks/show.html'
    #   controller: 'tracksShowController'
