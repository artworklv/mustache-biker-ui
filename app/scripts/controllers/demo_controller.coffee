class Demo extends Controller
  constructor: ($scope, $filter, $http, $stateParams, mapService, trackFixtureService, carFixtureService, mapTrackService, trackChartService, heatMapService) ->
    $scope.trackId = 1 * $stateParams.id

    console.log "Track id: #{$scope.trackId}"

    $scope.tracks = []
    $scope.track = {}
    $scope.trackChart = [{key: 'a', values: [[1,2], [3,4]]}]

    $scope.stats = {}

    $scope.center =
      lat: 52.5097052402  # latlngs[latlngs.length / 2].lat
      lng: 13.3931966499   # latlngs[latlngs.length / 2].lng
      zoom: 15

    $scope.map = mapService.build()

    $http(method: 'GET', url: "http://api.mustachebiker.in/tracks").then (data) ->
      _.each data.data, (x) ->
        track =
          id: x
          name: "Track #{x}"

        $scope.tracks.push(track)


    $http(method: 'GET', url: "http://api.mustachebiker.in/stats?id=#{$scope.trackId}").then (data) ->
      console.log("Got stats")
      console.log(data.data)
      $scope.stats = data.data

    $http(method: 'GET', url: "http://api.mustachebiker.in/track?id=#{$scope.trackId}").then (data) ->

      $scope.track = data.data

      heatmap = heatMapService.build(data.data)

      _map = mapService.build()

      layers = _map.layers

      layers.overlays = {
        heatmap:
          name: "Heat Map"
          type: "heatmap"
          data: heatmap
          visible: true
          layerParams: {}
          layerOptions: {size: 20}
      }

      $scope.map.layers = layers

      $scope.trackChart = trackChartService.build(data.data)







    # $scope.map = mapService.build()

    # $scope.mapTracks = {}
    # $scope.mapTracksCenter = {}


    # $scope.tracks     = []
    # $scope.track      = {}
    # $scope.trackChart = [{key: 'a', values: [[1,2], [3,4]]}]


    # trackFixtureService.all($scope.vehicleId).then (tracks) ->
    #   $scope.tracks = tracks

    # carFixtureService.find($scope.vehicleId)
    # .then (vehicle) ->
    #   $scope.vehicle = vehicle
    # .catch (error) ->
    #   console.log(error)

    # trackFixtureService.find($scope.trackId, $scope.vehicleId)
    # .then (track) ->
    #   $scope.track = track
    #   mp = mapTrackService.build(track)
    #   $scope.mapTracks = mp.tracks
    #   $scope.mapTracksCenter = mp.center
    #   $scope.trackChart = trackChartService.build(track)

    # .catch (error) ->
    #   console.log(error)


    # $http(method: 'GET', url: "http://api.mustachebiker.in/track?id=10").then (data) ->
    #   console.log data
